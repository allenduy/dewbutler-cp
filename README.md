This is an on-going personal project.

# DewButler

A chatbot system for Twitch.tv, which will be completely hosted in the (AWS) cloud, as opposed to locally-run server bots that requires an installation on one's PC.

The goal is to make it as simple and user-friendly as possible, but also customizable, encapsulating the best features of currently available bot systems.

## DewButler Control Panel

Developed using tools such as ReactJS, Redux, Webpack2, Babel, Sass, and FontAwesome, this is only the front-end half of the web application allowing streamers and viewers to control, maintain, and interact with their Butler. The back-end is currently hidden away in the depths... of a privated repository ^^

Image sources are using the great, free-to-use, Unsplash.com
