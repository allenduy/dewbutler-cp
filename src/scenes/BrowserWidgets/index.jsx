import React from 'react';
import { Switch, Route } from 'react-router-dom';

import './style.scss';
import Alerts from './scenes/Alerts';
import Chat from './scenes/Chat';
import Custom from './scenes/Custom';
import Events from './scenes/Events';
import Music from './scenes/Music';

class BrowserWidgets extends React.Component {
  render() {
    return (
      <div styleName="container">
        <Switch>
          <Route path={ `${ this.props.match.url }/alerts` } component={ Alerts } />
          <Route path={ `${ this.props.match.url }/chat` } component={ Chat } />
          <Route path={ `${ this.props.match.url }/custom` } component={ Custom } />
          <Route path={ `${ this.props.match.url }/events` } component={ Events } />
          <Route path={ `${ this.props.match.url }/music` } component={ Music } />
        </Switch>
      </div>
    );
  }
}

export default BrowserWidgets;
