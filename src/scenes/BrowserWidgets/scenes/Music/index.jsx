import React from 'react';

import './style.scss';
import Visual from './components/Visual';
import Text from './components/Text';

class Music extends React.Component {
  render() {
    return (
      <div styleName="container">
        <Visual />
        <Text title="If it's a YouTube video, the title can get pretty long." artist="Some Artist or User" />
      </div>
    );
  }
}

export default Music;
