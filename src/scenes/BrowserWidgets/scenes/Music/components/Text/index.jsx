import React from 'react';

import './style.scss';

class Text extends React.Component {
  renderText(n) {
    return (
      <div className={ `text n${n}` }>
        <div className="marquee">
          <div className="content">
            <span className="title">
              { this.props.title }
            </span>
            <span className="artist">
              { this.props.artist }
            </span>
          </div>
        </div>
      </div>
    );
  }

  render() {
    return (
      <div styleName="container">
        { this.renderText(0) }
        { this.renderText(1) }
        { this.renderText(2) }
      </div>
    );
  }
}

export default Text;
