import React from 'react';
import { connect } from 'react-redux';
import { Switch, Route } from 'react-router-dom';

import './style.scss';
import Landing from './scenes/Landing';
import Dashboard from './scenes/Dashboard';
import Settings from './scenes/Settings';
import SignIn from './scenes/SignIn';
import Information from './scenes/Information';
import Profile from './scenes/Profile';

class Routes extends React.PureComponent {
  render() {
    return (
      <div styleName="container">
        <Switch>
          {/*Landing is a straight-forward description and preview of DewButler, monthly poll, and login*/}
          <Route exact path="/" component={ Landing } />
          <Route path="/dashboard" component={ Dashboard } />
          <Route path="/settings" component={ Settings } />
          <Route path="/signin" component={ SignIn } />

          {/*Information will contain terms, policies, faq, etc.*/}
          <Route path="/information" component={ Information } />

          {/*Profile is a public page, visible to all viewers (provide with !info "Interactive profile at https://butler.dew.studio/#[user:name]")*/}
          <Route path="/:displayname" component={ Profile } />
        </Switch>
      </div>
    );
  }
}

export default Routes;
