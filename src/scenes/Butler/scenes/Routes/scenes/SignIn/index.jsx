import React from 'react';
import { connect } from 'react-redux';
import { parse } from 'query-string';

import { setPage } from '../../actions';

import './style.scss';

const mapDispatchToProps = (dispatch, props) => {
  return {
    setPage: () => {
      dispatch(setPage(props.location.pathname))
    }
  }
}

class SignIn extends React.Component {
  constructor(props) {
    super(props);
    
    this.props.match.params = Object.assign(
      this.props.match.params,
      parse(this.props.location.search)
    );
  }

  componentWillMount() {
    this.props.setPage();
  }

  // a button should contain information of target page and selected state (color)
  // states: logged in [viewer, streamer, admin, mod], logged out
  render() {
    return (
      <div>
        Code: { this.props.match.params.code }
        { this.props.match.params.code }
      </div>
    );
  }
}

export default connect(null, mapDispatchToProps)(SignIn);
