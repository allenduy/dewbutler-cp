import React from 'react';
import { connect } from 'react-redux';

import { setPage } from '../../actions';

import './style.scss';

const mapDispatchToProps = (dispatch, props) => {
  return {
    setPage: () => {
      dispatch(setPage(props.location.pathname))
    }
  }
}

class Profile extends React.Component {
  componentWillMount() {
    this.props.setPage();
  }

  render() {
    return (
      <div>
        Profile: { this.props.match.params.displayname }
        <br />
        Path: { this.props.match.path }
        <br />
        Url: { this.props.match.url }
      </div>
    );
  }
}

export default connect(null, mapDispatchToProps)(Profile);
