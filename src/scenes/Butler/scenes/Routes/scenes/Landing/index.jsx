import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { setPage } from '../../actions';

import './style.scss';
import Button from 'components/Button';

const mapDispatchToProps = (dispatch, props) => {
  return {
    setPage: () => {
      dispatch(setPage(props.location.pathname))
    }
  }
}

class Landing extends React.Component {
  componentWillMount() {
    this.props.setPage();
  }

  render() {
    return (
      <div styleName="page">

        <div styleName="head">
          <div styleName="text">
            <span styleName="title">DewButler</span>
            <br/>
            <span styleName="caption">coming soon.</span>
            <br/>
            <br/>
            <div styleName="subBlock">
              {/*<a target="_self" href="https://api.twitch.tv/kraken/oauth2/authorize?client_id=uas0z23x6lglkh84l9grh9bnn3h8jl&redirect_uri=http://localhost:3000/signin&response_type=token&scope=chat_login&force_verify=true">*/}
                <Button className="lg">sign in with twitch | go to dashboard</Button>
              {/*</a>*/}
              <p>The community has raised:</p>
              <p>0USD total</p>
              <p>0USD this month</p>
            </div>
          </div>
        </div>

        <div styleName="block">
          <h1>What is DewButler?</h1>
          <p styleName="blockBody">
            DewButler is a cloud-based live-streaming chatbot developed by <a href="https://twitch.tv/allendewy">allendewy</a> (Dew Studio), which will be available for Twitch.tv users.
          </p>
          <h4>Charity of the Month</h4>
          <p styleName="blockBody">
            When the time is right, and DewButler is ready for release, I want to use this service as an opportunity to bring awareness to charities, voted monthly in polls by users of the community! I will have a monthly counter of how much has been raised, and send a donation, given the help of our users and viewers. Of course, if you prefer to donate directly, I encourage you to, but DewButler will simply be a more accessible way to give what you can spare.
          </p>
        </div>

        <div styleName="separator"></div>

        <div styleName="block">
          <h1>Status</h1>
          <p>
            DewButler is currently in development. A private alpha will eventually (as in no one knows when, but it will happen.. eventually) be available for only a select number of participants.
          </p>
        </div>

        <div styleName="separator"></div>

        <div styleName="block">
          <h1>Expected Features</h1>
          <p styleName="blockBody">
            TBD.
          </p>
        </div>

        <div styleName="foot">
          <div styleName="subBlock">
            <Button className="lg">get started</Button>
          </div>
        </div>

      </div>
      )
  }
}

export default connect(null, mapDispatchToProps)(Landing);
