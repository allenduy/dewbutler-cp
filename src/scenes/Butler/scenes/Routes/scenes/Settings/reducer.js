import { combineReducers } from 'redux';
import { reducer as routes } from './scenes/Routes/reducer';

export const reducer = combineReducers({
  routes
});
