import React from 'react';
import { Link } from 'react-router-dom';

import './style.scss';
import MenuButton from './components/MenuButton';

class SecondaryMenu extends React.Component {
  buttons(list) {
    return (
      list.map((item) => {
        return (
          <MenuButton
            key={ item.label }
            path={ item.path }
            label={ item.label }
            />
        );
      })
    );
  }

  render() {
    return (
      <div styleName="container">
        {
          this.buttons([
            {
              label: 'account',
              path: `${ this.props.match.url }`
            },
            {
              label: 'alerts',
              path: `${ this.props.match.url }/alerts`
            },
            {
              label: 'commands',
              path: `${ this.props.match.url }/commands`
            },
            {
              label: 'loyalty points',
              path: `${ this.props.match.url }/points`
            },
            {
              label: 'moderation',
              path: `${ this.props.match.url }/moderation`
            },
            {
              label: 'music',
              path: `${ this.props.match.url }/music`
            },
            {
              label: 'quotes',
              path: `${ this.props.match.url }/quotes`
            },
            {
              label: 'ranks',
              path: `${ this.props.match.url }/ranks`
            },
          ])
        }
      </div>
    );
  }
}

export default SecondaryMenu;
