import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import './style.scss';
import Button from 'components/Button';

const mapStateToProps = (state, props) => {
  return {
    active: (props.path !== '/settings') ?
      state.app.butler.settings.routes.active.includes(props.path) :
      state.app.butler.settings.routes.active === props.path
  }
}

class MenuButton extends React.PureComponent {
  render() {
    return (
      <div styleName="container">
        <Link to={ this.props.path }>
          <Button className={ `fw md${ this.props.active ? ' active' : '' }` }>
            { this.props.label }
          </Button>
        </Link>
      </div>
    );
  }
}

export default connect(mapStateToProps)(MenuButton);