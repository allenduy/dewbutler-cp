import { SET_PAGE } from './actions';

const initialState = {
  active: '/settings',
};

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_PAGE:
      return Object.assign({}, state, {
        active: action.page
      });
    default:
      return state;
  }
};
