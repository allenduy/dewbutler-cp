import React from 'react';
import { connect } from 'react-redux';

import { setPage } from '../../actions';

import './style.scss';

const mapDispatchToProps = (dispatch, props) => {
  return {
    setPage: () => {
      dispatch(setPage(props.location.pathname))
    }
  }
}

class Music extends React.Component {
  componentWillMount() {
    this.props.setPage();
  }

  render() {
    return (
      <div>
        Music
      </div>
    );
  }
}

export default connect(null, mapDispatchToProps)(Music);
