import React from 'react';
import { Switch, Route } from 'react-router-dom';

import './style.scss';
import Account from './scenes/Account';
import Alerts from './scenes/Alerts';
import Commands from './scenes/Commands';
import Moderation from './scenes/Moderation';
import Music from './scenes/Music';
import Points from './scenes/Points';
import Quotes from './scenes/Quotes';
import Ranks from './scenes/Ranks';

class Routes extends React.PureComponent {
  render() {
    return (
      <div styleName="container">
        <Switch>
          <Route exact
            path={ `${ this.props.match.url }` }
            component={ Account }
            />
          <Route
            path={ `${ this.props.match.url }/alerts` }
            component={ Alerts }
            />
          <Route
            path={ `${ this.props.match.url }/commands` }
            component={ Commands }
            />
          <Route
            path={ `${ this.props.match.url }/points` }
            component={ Points }
            />
          <Route
            path={ `${ this.props.match.url }/moderation` }
            component={ Moderation }
            />
          <Route
            path={ `${ this.props.match.url }/music` }
            component={ Music }
            />
          <Route
            path={ `${ this.props.match.url }/quotes` }
            component={ Quotes }
            />
          <Route
            path={ `${ this.props.match.url }/ranks` }
            component={ Ranks }
            />
        </Switch>
      </div>
    );
  }
}

export default Routes;
