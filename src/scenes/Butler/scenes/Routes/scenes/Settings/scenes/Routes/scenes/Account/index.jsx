import React from 'react';
import { connect } from 'react-redux';

import { setPage } from '../../actions';

import './style.scss';

const mapDispatchToProps = (dispatch, props) => {
  return {
    setPage: () => {
      dispatch(setPage(props.location.pathname))
    }
  }
}

class Account extends React.Component {
  componentWillMount() {
    this.props.setPage();
  }

  render() {
    return (
      <div styleName="container">
        Welcome, {`{user.name}`}!
        <p>
          <h3>Custom Bot Account</h3>
          <h3>Subscriptions</h3>
          <h3>Enable Blah</h3>
        </p>
      </div>
    );
  }
}

export default connect(null, mapDispatchToProps)(Account);
