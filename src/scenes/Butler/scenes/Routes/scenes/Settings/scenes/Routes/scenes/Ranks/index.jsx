import React from 'react';
import { connect } from 'react-redux';

import { setPage } from '../../actions';

import './style.scss';

const mapDispatchToProps = (dispatch, props) => {
  return {
    setPage: () => {
      dispatch(setPage(props.location.pathname))
    }
  }
}

class Ranks extends React.Component {
  componentWillMount() {
    this.props.setPage();
  }

  render() {
    return (
      <div>
        Ranks
      </div>
    );
  }
}

export default connect(null, mapDispatchToProps)(Ranks);
