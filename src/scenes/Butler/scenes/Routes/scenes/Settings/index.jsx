import React from 'react';
import { connect } from 'react-redux';
import { Route } from 'react-router-dom';

import { setPage } from '../../actions';

import './style.scss';
import SecondaryMenu from './components/SecondaryMenu';
import Routes from './scenes/Routes';

const mapDispatchToProps = (dispatch, props) => {
  return {
    setPage: () => {
      dispatch(setPage(props.location.pathname))
    }
  }
}

class Settings extends React.PureComponent {
  componentWillMount() {
    this.props.setPage();
  }

  render() {
    return (
      <div>
        <Route component={ SecondaryMenu } />
        <Route component={ Routes } />
      </div>
    );
  }
}

export default connect(null, mapDispatchToProps)(Settings);

  /*<ul>
    <li>
      Dashboard
      <ul>
        <li>stream preview (muted)</li>
        <li>chat embedded</li>
        <li>other widgets (poll, music, etc)</li>
        <li>stream settings (game, title, community, etc)</li>
        <li>stream stats (mods available, on/offline, uptime, viewer count)</li>
        <li>event feed (follow, purchase, sub, etc.); persistant local cache?</li>
      </ul>
    </li>
    <li>
      Settings menu
    </li>
    <li>
      Profile
      <ul>
        <li>viewer stats (points accumulated, time in chat, follow time)</li>
        <li>video+chat embedded</li>
        <li>live events (streamer can display raid target, poll answers, giveaway winners, etc)</li>
        <li>stream stats (on/offline, uptime, viewer count)</li>
        <li>points store, donation form+reward store, available (custom) commands+how to use</li>
      </ul>
    </li>
    <li>
      Information
    </li>
  </ul>*/