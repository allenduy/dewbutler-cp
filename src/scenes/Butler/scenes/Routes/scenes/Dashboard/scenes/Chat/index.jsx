import React from 'react';

import './style.scss';

class Chat extends React.PureComponent {
  render() {
    return (
      <div className="body" styleName="container">
        <iframe src="https://www.twitch.tv/allendewy/chat">
        </iframe>
      </div>
    );
  }
}

export default Chat;
