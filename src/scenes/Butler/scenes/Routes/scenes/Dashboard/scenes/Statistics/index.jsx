import React from 'react';

import './style.scss';

class Statistics extends React.PureComponent {
  render() {
    return (
      <div className="body">
        panel to display viewer, streaming, on/offline, chat stats, etc.
      </div>
    );
  }
}

export default Statistics;
