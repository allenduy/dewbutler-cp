import React from 'react';

import './style.scss';

class Channel extends React.PureComponent {
  render() {
    return (
      <div className="body" styleName="container">
        edit stream info; current game, stream title (save feature for previous titles related to selected game)
      </div>
    );
  }
}

export default Channel;
