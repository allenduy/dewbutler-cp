import React from 'react';

import './style.scss';

class Bot extends React.PureComponent {
  render() {
    return (
      <div className="body">
        panel to tell bot to join or leave the channel.
      </div>
    );
  }
}

export default Bot;
