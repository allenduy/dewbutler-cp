import React from 'react';

import './style.scss';

class Shortcuts extends React.PureComponent {
  render() {
    return (
      <div className="body">
        run favorite commands (ie sellout) or open favorite links (ie other streaming tools - streamlabs)
      </div>
    );
  }
}

export default Shortcuts;
