import React from 'react';

import './style.scss';

class Video extends React.PureComponent {
  render() {
    return (
      <div className="body" styleName="container">
        <div className="ratio">
          <iframe src="https://player.twitch.tv/?channel=allendewy&muted=true">
          </iframe>
        </div>
      </div>
    );
  }
}

export default Video;
