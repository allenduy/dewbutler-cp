import React from 'react';

import './style.scss';

class Poll extends React.PureComponent {
  render() {
    return (
      <div className="body">
        start/end temporary polls; results are not saved anywhere (maybe, TBD)
      </div>
    );
  }
}

export default Poll;
