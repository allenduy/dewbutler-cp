import React from 'react';

import './style.scss';

class Music extends React.PureComponent {
  render() {
    return (
      <div styleName="container" className="body">
        <div>play, pause, skip</div>
        <span>alternating</span>
        <span>colored</span>
        <span>rows</span>
      </div>
    );
  }
}

export default Music;
