import React from 'react';

import './style.scss';
import FA from 'components/FA';

class Song extends React.PureComponent {
  render() {
    return (
      <span>
        { this.props.children }
        <FA icon="time-circle" />
      </span>
    );
  }
}