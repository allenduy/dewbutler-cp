import React from 'react';

import './style.scss';

class Raid extends React.PureComponent {
  render() {
    return (
      <div className="body">
        for users who enjoy giving raids, this will be a quick way to find who's online that you follow, or (maybe) who's streaming in the game category.
      </div>
    );
  }
}

export default Raid;
