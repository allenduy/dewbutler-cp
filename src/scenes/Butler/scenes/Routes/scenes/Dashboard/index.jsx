import React from 'react';
import { connect } from 'react-redux';

import { setPage } from '../../actions';

import './style.scss';
import ToggleMenu from './components/ToggleMenu';
import Bot from './scenes/Bot';
import Video from './scenes/Video';
import Chat from './scenes/Chat';
import Channel from './scenes/Channel';
import Music from './scenes/Music';
import Poll from './scenes/Poll';
import Quotes from './scenes/Quotes';
import Raid from './scenes/Raid';
import Shortcuts from './scenes/Shortcuts';
import Statistics from './scenes/Statistics';
import Panel from 'components/Panel';

const mapDispatchToProps = (dispatch, props) => {
  return {
    setPage: () => {
      dispatch(setPage(props.location.pathname))
    }
  }
}

class Dashboard extends React.Component {
  componentWillMount() {
    this.props.setPage();
  }

  render() {
    return (
      <div>
        <ToggleMenu />
        <div styleName="content">
          <Panel icon="power-off" label="bot"><Bot/></Panel>
          <Panel icon="twitch" label="channel"><Channel /></Panel>
          <Panel icon="video-camera" label="video" className="y2"><Video /></Panel>
          <Panel icon="comments" label="chat" className="y4"><Chat /></Panel>
          <Panel icon="bar-chart" label="stats"><Statistics /></Panel>
          <Panel icon="music" label="music" className="y2"><Music /></Panel>
          <Panel icon="pie-chart" label="poll"><Poll /></Panel>
          <Panel icon="quote-right" label="quotes"><Quotes /></Panel>
          <Panel icon="rocket" label="raid"><Raid /></Panel>
          <Panel icon="link" label="shortcuts"><Shortcuts /></Panel>
        </div>
      </div>
    );
  }
}

export default connect(null, mapDispatchToProps)(Dashboard);
