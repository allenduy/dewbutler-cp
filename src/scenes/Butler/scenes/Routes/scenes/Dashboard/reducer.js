import { combineReducers } from 'redux';

import { reducer as panel } from './components/ToggleButton/reducer';

export const reducer = combineReducers({
  panel
});
