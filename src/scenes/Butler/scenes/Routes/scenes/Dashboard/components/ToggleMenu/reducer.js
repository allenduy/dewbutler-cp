import { ScrollState } from './actions';

const initialState = {
  scrollLeft: false,
  scrollRight: false
};

export const reducer = (state = initialState, action) => {
  switch (action.state) {
    case ScrollState.LEFT:
      return Object.assign({}, state, {
        scrollLeft: true,
        scrollRight: false
      });
    case ScrollState.RIGHT:
      return Object.assign({}, state, {
        scrollLeft: false,
        scrollRight: true
      });
    case ScrollState.STOP:
      return Object.assign({}, state, {
        scrollLeft: false,
        scrollRight: false
      });
    default:
      return state;
  }
}
