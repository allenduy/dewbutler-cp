import React from 'react';
import { connect } from 'react-redux';

import { togglePanel } from '../../actions';

import Button from 'components/Button';
import FA from 'components/FA';

const mapStateToProps = (state, props) => {
  return {
    active: state.app.butler.dashboard.panel[props.label]
  }
}

const mapDispatchToProps = (dispatch, props) => {
  return {
    onClick: () => dispatch(togglePanel(props.label))
  }
}

class ToggleButton extends React.PureComponent {
  render() {
    return (
      <div onClick={ this.props.onClick }>
        <Button className={ `md icn${ this.props.active ? ' active' : '' }` }>
          <FA icon={ this.props.icon } />
        </Button>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ToggleButton);
