import { TOGGLE_PANEL } from './actions';

const initialState = {
  bot: true,
  channel: true,
  chat: true,
  music: true,
  poll: true,
  quotes: true,
  raid: true,
  shortcuts: true,
  stats: true,
  video: true,
};

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE_PANEL:
      return Object.assign({}, state, {
        [action.panel]: !state[action.panel]
      });
    default:
      return state;
  }
};
