export const TOGGLE_PANEL = 'TOGGLE_PANEL';

export const togglePanel = panel => {
  return {
    type: TOGGLE_PANEL,
    panel
  }
}