import React from 'react';
import { connect } from 'react-redux';

import './style.scss';
import Button from 'components/Button';
import Label from 'components/Label';
import Tooltip from 'components/Tooltip';
import FA from 'components/FA';
import ToggleButton from '../ToggleButton';

const mapStateToProps = state => {
  return {
    scrollLeft: state.left,
    scrollRight: state.right
  }
}

class ToggleButtons extends React.Component {
  componentDidMount() {
    
  }

  // todo: dispatch toggle panel from buttons
  buttons(list) {
    return (
      list.map((item) => {
        return (
          <Tooltip key={ item.label } text={ item.label } position="bottom">
            <ToggleButton icon={ item.icon } label={ item.label } />
          </Tooltip>
        );
      })
    );
  }

  render() {
    return (
      <div className="container">
        <Label>toggles</Label>
        {
          this.buttons([
            { label: 'bot', icon: 'power-off' },
            { label: 'channel', icon: 'twitch' },
            { label: 'video', icon: 'video-camera' },
            { label: 'chat', icon: 'comments' },
            { label: 'stats', icon: 'bar-chart' },
            { label: 'music', icon: 'music' },
            { label: 'raid', icon: 'rocket' },
            { label: 'poll', icon: 'pie-chart' },
            { label: 'settings', icon: 'cog' },
            { label: 'bot2', icon: 'android' },
            { label: 'points', icon: 'money' },
            { label: 'giveaways', icon: 'trophy' },
            { label: 'quotes', icon: 'quote-right' },
            { label: 'shortcuts', icon: 'link', },
          ])
        }
      </div>
    );
  }
}

export default connect(mapStateToProps)(ToggleButtons);