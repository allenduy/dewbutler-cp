import React from 'react';
import { connect } from 'react-redux';

import { scroll, ScrollState } from './actions.js';

import './style.scss';
import ToggleButtons from '../ToggleButtons';
import FA from 'components/FA';

const mapDispatchToProps = dispatch => {
  return {
    onLMouseDown: () => dispatch(scroll(ScrollState.LEFT)),
    onRMouseDown: () => dispatch(scroll(ScrollState.RIGHT)),
    onMouseUp: () => dispatch(scroll(ScrollState.STOP))
  }
}

class ToggleMenu extends React.Component {
  render() {
    return (
      <div styleName="container">
        <div styleName="scroller">
          <span className="left"
              onMouseDown={ this.props.onLMouseDown }
              onMouseUp={ this.props.onMouseUp }
              >
            <FA icon="angle-left" className="fa-lg" />
          </span>
          <span className="right"
              onMouseDown={ this.props.onRMouseDown }
              onMouseUp={ this.props.onMouseUp }
              >
            <FA icon="angle-right" className="fa-lg" />
          </span>
        </div>
        <ToggleButtons />
      </div>
    );
  }
}

export default connect(null, mapDispatchToProps)(ToggleMenu);
