export const SCROLL = 'SCROLL';

export const ScrollState = {
  LEFT: 'LEFT',
  RIGHT: 'RIGHT',
  STOP: 'STOP'
}

export const scroll = state => {
  return {
    type: SCROLL,
    state
  }
}
