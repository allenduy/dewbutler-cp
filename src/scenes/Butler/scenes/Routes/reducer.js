import { combineReducers } from 'redux';
import { reducer as dashboard } from './scenes/Dashboard/reducer'
import { reducer as settings } from './scenes/Settings/reducer'
import { SET_PAGE } from './actions';

const initialState = {
  active: '/',
};

const routes = (state = initialState, action) => {
  switch (action.type) {
    case SET_PAGE:
      return Object.assign({}, state, {
        active: action.page
      });
    default:
      return state;
  }
};

export const reducer = combineReducers({
  dashboard,
  settings,
  routes
});
