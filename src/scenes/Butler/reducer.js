import { combineReducers } from 'redux';
import { reducer as butler } from './scenes/Routes/reducer'

export const reducer = combineReducers({
  butler
});
