import React from 'react';
import { Route } from 'react-router-dom';
import { connect } from 'react-redux';

import './style.scss';
import Menu from './components/Menu';
import Routes from './scenes/Routes';

const mapStateToProps = state => {
  console.log(state);
  return {}
}

class Butler extends React.Component {
  componentWillMount() {
    // check if logged in, dispatch state, or not
  }

  render() {
    return (
      <div styleName="container">
        <Route component={ Menu } />
        <Route component={ Routes } />
      </div>
    );
  }
}

export default connect(mapStateToProps)(Butler);
