import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import './style.scss';
import Button from 'components/Button';
import FA from 'components/FA';
import Label from 'components/Label';

const mapStateToProps = (state, props) => {
  return {
    active: (props.path !== '/') ?
      state.app.butler.routes.active.includes(props.path) :
      state.app.butler.routes.active === props.path
  }
}

class MenuButton extends React.PureComponent {
  render() {
    return (
      <div styleName="container">
        <Link to={ this.props.path }>
          <Button className={ `fw md icn${ this.props.active ? ' active' : '' }` }>
            <FA className="fa-fw fa-lg" icon={ this.props.icon } />
            <Label>{ this.props.label }</Label>
          </Button>
        </Link>
      </div>
    );
  }
}

export default connect(mapStateToProps)(MenuButton);
