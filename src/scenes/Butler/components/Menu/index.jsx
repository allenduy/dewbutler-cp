import React from 'react';
import { connect } from 'react-redux';

import './style.scss';
import MenuButton from './components/MenuButton';

const mapStateToProps = (state, props) => {
  return {
    active: props.location.pathname
  }
}

class Menu extends React.Component {
  buttons(list) {
    return (
      list.map((item) => {
        return (
          <MenuButton
            key={ item.label }
            path={ item.path }
            icon={ item.icon }
            label={ item.label }
            />
        );
      })
    );
  }

  // a button should contain information of target page and selected state (color)
  // states: logged in [viewer, streamer, admin, mod], logged out
  render() {
    return (
      <div styleName="container">
        <div styleName="sceneMenu">
          {
            this.buttons([
              { path: '/', icon: 'android', label: 'dewbutler' },
              { path: '/dashboard', icon: 'th', label: 'dashboard' },
              { path: '/settings', icon: 'cogs', label: 'settings' },
              { path: '/information', icon: 'info', label: 'information' },
            ])
          }
        </div>
        <div styleName="accountMenu">
          <MenuButton
            path="/allendewy"
            icon="user"
            label="VeryLongUserNameForTestingTheEllipses"
            />
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps)(Menu);
