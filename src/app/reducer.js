import { combineReducers } from 'redux';
import { reducer as app } from 'scenes/Butler/reducer';

export const reducer = combineReducers({
  app
});
