import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';

import 'font-awesome/css/font-awesome.css';
import 'whatwg-fetch';

import './style.scss';
import Butler from 'scenes/Butler';
import BrowserWidgets from 'scenes/BrowserWidgets';

class App extends React.Component {
  render() {
    return (  /* wrapper/container */
      <BrowserRouter>
        <Switch>
          <Route path="/widgets" component={ BrowserWidgets } />
          <Route path="/" component={ Butler } />
          <Redirect from="/dewbutler-cp" to="/" />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
