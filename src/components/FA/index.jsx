import React from 'react';

class FA extends React.PureComponent {
  render() {
    return (
      <span className={ `fa fa-${ this.props.icon } ${ this.props.className }` }></span>
    );
  }
}

export default FA;
