import React from 'react';

import './style.scss';

class Tooltip extends React.PureComponent {
  render() {
    return (
      <div styleName="container">
        <span className={ `tooltip ${ this.props.position }` }>
          { this.props.text }
        </span>
        <span className="content">{ this.props.children }</span>
      </div>
    );
  }
}

export default Tooltip;
