import React from 'react';

import './style.scss';

class Button extends React.Component {
  render() {
    return (
      <div className={ `button ${ this.props.className }` }
          styleName="container">
        <span>{ this.props.children }</span>
      </div>
    );
  }
}

export default Button;
