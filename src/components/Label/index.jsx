import React from 'react';

import './style.scss';

class Label extends React.Component {
  render() {
    return (
      <div className="label" styleName="container">
        <span>{ this.props.children }</span>
      </div>
    );
  }
}

export default Label;
