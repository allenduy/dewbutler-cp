import React from 'react';
import { connect } from 'react-redux';

import './style.scss';
import Label from '../Label';
import FA from '../FA';

const mapStateToProps = (state, props) => {
  return {
    active: state.app.butler.dashboard.panel[props.label]
  }
}

class Panel extends React.Component {
  render() {
    return (this.props.active) ? (
      <div className={ this.props.className } styleName="container">
        <Label>
          <FA icon={ this.props.icon } />{ this.props.label }
          <FA icon="close" className="close" />
        </Label>
        { this.props.children }
      </div>
    ) : null;
  }
}

export default connect(mapStateToProps)(Panel);
